PREDICATES
	nondeterm sum(integer, integer)
CLAUSES
	sum(1,1):-!.
	sum(N, S):- N1 = N - 1, sum(N1, S1), S = N + S1.
GOAL
	write("N = "),readint(N),sum(N,S),write(" F = ",S),nl.