PREDICATES
	nondeterm sumfact(integer, integer)
	nondeterm fact(integer, integer)
CLAUSES
	fact(0,1):-!.
	fact(N, F):- N1 = N - 1, fact(N1, F1), F = N * F1.
	
	sumfact(1,1):-!.
	sumfact(N,S):- N1 = N - 2, fact(N, F), sumfact(N1, S1), S = S1 + F.
GOAL
	sumfact(9,S), write("S = ",S),nl.
	%fact(3,F), write("F = ",F),nl.