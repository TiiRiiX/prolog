PREDICATES
	nondeterm mult(real, real)
CLAUSES
	mult(2,2):-!.
	mult(N, S):- N1 = N - 2, mult(N1, S1), S = N * S1,write(S),nl.
GOAL
	mult(26,S),write(" Mult = ",S),nl.