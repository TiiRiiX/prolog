DOMAINS
	list = integer*
PREDICATES
	genl(integer, list)	
	mult_list(list, integer)
CLAUSES
	genl(-1,[]):-!.
	genl(N, [N|L]):- N1 = N - 2, genl(N1, L).
	mult_list([],1).
	mult_list([X|L],S):-mult_list(L,S1),S=S1*X.
GOAL
	genl(7, L),mult_list(L,M).