DOMAINS
	list = integer*
PREDICATES
	genl(integer, integer, list)
	nondeterm ins(integer, list, list)
	nondeterm del(integer, list, list)
CLAUSES
	del(X,[X|L],L).
	del(X,[Y|L],[Y|L1]):-del(X,L,L1).
	ins(X,L1,L):-L = [X|L1].	
	genl(N2, N2,[]):-!.
	genl(N1, N2,[N1|L]):- N1 <= N2 + 2, N = N1 + 2, genl(N, N2, L).
GOAL
	genl(2, 12, L), write(L),nl, write(" X = "),
	readint(X),del(X,L,L1), write(L1),nl, ins(5,L1,L2),write(L2),nl, fail.