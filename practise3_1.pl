summa([], 0) :- !.
summa([Head | Tail], S) :- summa(Tail, S1), S is S1 + Head.